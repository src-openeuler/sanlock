Name:             sanlock
Version:          3.9.4
Release:          1
Summary:          A shared storage lock manager
License:          GPL-2.0-only AND GPL-2.0-or-later AND LGPL-2.0-or-later
URL:              https://pagure.io/sanlock/
Source0:          https://releases.pagure.org/sanlock/%{name}-%{version}.tar.gz

Patch1:           Add-sanlk-reset-command-t-options.patch

BuildRequires:    libblkid-devel libaio-devel python3 python3-devel systemd-units gcc
BuildRequires:    libuuid-devel python3-setuptools
Requires(pre):    shadow
Requires(post):   systemd-units systemd-sysv
Requires(preun):  systemd-units
Requires(postun): systemd-units

Provides:         sanlock-lib = %{version}-%{release}
Obsoletes:        sanlock-lib < %{version}-%{release}


%description
The sanlock daemon manages leases for applications running on a cluster of hosts
with shared storage. All lease management and coordination is done through reading
and writing blocks on the shared storage.


%package      devel
Summary:      libraries and header files for sanlock
Requires:     %{name} = %{version}-%{release}

%description  devel
The sanklock-devel package contains libraries and header files
for developing applications that use sanlock.

%package      help
Summary:      maninfo for sanlock
Requires:     %{name} = %{version}-%{release}

%description  help
The sanklock-help package contains maninfo for sanlock.
	
%package        -n python3-sanlock
%{?python_provide:%python_provide python3-sanlock}
Summary:        Python bindings for the sanlock library
Requires:       %{name} = %{version}-%{release}
 
%description    -n python3-sanlock
The python3-sanlock package contains a module that
permits applications written in the Python programming language
to use the interface supplied by the sanlock library.

%package      -n   sanlk-reset
Summary:      Host reset daemon and client using sanlock
Requires:     sanlock = %{version}-%{release}

%description  -n sanlk-reset
The sanlk-reset package contains the reset daemon and client.
A cooperating host running the daemon can be reset by a host
running the client, so long as both maintain access to a
common sanlock lockspace.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%set_build_flags
CFLAGS=$RPM_OPT_FLAGS make -C wdmd
CFLAGS=$RPM_OPT_FLAGS make -C src
CFLAGS=$RPM_OPT_FLAGS make -C python PY_VERSION=3
CFLAGS=$RPM_OPT_FLAGS make -C reset

%install
make -C src    install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT
make -C wdmd   install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT
make -C python install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT PY_VERSION=3
make -C reset  install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT

install -D -m 0644 init.d/sanlock.service.native $RPM_BUILD_ROOT/%{_unitdir}/sanlock.service
install -D -m 0755 init.d/systemd-wdmd $RPM_BUILD_ROOT/usr/lib/systemd/systemd-wdmd
install -D -m 0644 init.d/wdmd.service $RPM_BUILD_ROOT/%{_unitdir}/wdmd.service
install -D -m 0644 init.d/sanlk-resetd.service $RPM_BUILD_ROOT/%{_unitdir}/sanlk-resetd.service
install -D -m 0644 src/logrotate.sanlock $RPM_BUILD_ROOT/etc/logrotate.d/sanlock
install -D -m 0644 src/sanlock.conf $RPM_BUILD_ROOT/etc/sanlock/sanlock.conf
install -D -m 0644 init.d/wdmd.sysconfig $RPM_BUILD_ROOT/etc/sysconfig/wdmd

install -p -D -m 0644 src/sanlock.sysusers $RPM_BUILD_ROOT/%{_sysusersdir}/sanlock.conf

install -Dd -m 0755 $RPM_BUILD_ROOT/etc/wdmd.d
install -Dd -m 0775 $RPM_BUILD_ROOT/%{_localstatedir}/run/sanlock
install -Dd -m 0775 $RPM_BUILD_ROOT/%{_localstatedir}/run/sanlk-resetd

%pre
getent group sanlock > /dev/null || /usr/sbin/groupadd -g 179 sanlock
getent passwd sanlock > /dev/null || /usr/sbin/useradd \
       -u 179 -c "sanlock" -s /sbin/nologin -r \
       -g 179 -d /var/run/sanlock sanlock
/usr/sbin/usermod -a -G disk sanlock

%post
%systemd_post wdmd.service sanlock.service
/sbin/ldconfig

%preun
%systemd_preun wdmd.service sanlock.service

%postun
%systemd_postun wdmd.service sanlock.service
/sbin/ldconfig


%files
%defattr(-,root,root,-)
/usr/lib/systemd/systemd-wdmd
%{_unitdir}/sanlock.service
%{_unitdir}/wdmd.service
%{_sbindir}/sanlock
%{_sbindir}/wdmd
%dir %{_sysconfdir}/wdmd.d
%dir %{_sysconfdir}/sanlock
%dir %attr(-,sanlock,sanlock) %{_localstatedir}/run/sanlock
%config(noreplace) %{_sysconfdir}/logrotate.d/sanlock
%config(noreplace) %{_sysconfdir}/sanlock/sanlock.conf
%config(noreplace) %{_sysconfdir}/sysconfig/wdmd
%{_sysusersdir}/sanlock.conf
%doc init.d/sanlock
%doc init.d/sanlock.service
%doc init.d/wdmd.service
%{_libdir}/libsanlock*.so.*
%{_libdir}/libwdmd.so.*

%files        help
%defattr(-,root,root,-)
%{_mandir}/man8/wdmd*
%{_mandir}/man8/sanlock*
%{_mandir}/man8/sanlk-reset*

%files        devel
%defattr(-,root,root,-)
%{_libdir}/libwdmd.so
%{_includedir}/wdmd.h
%{_libdir}/libsanlock*.so
%{_includedir}/sanlock*.h
%{_libdir}/pkgconfig/libsanlock*.pc

%files -n python3-sanlock
%defattr(-,root,root,-)
%{python3_sitearch}/sanlock_python-*.egg-info
%{python3_sitearch}/sanlock*.so

%files -n     sanlk-reset
%defattr(-,root,root,-)
%{_sbindir}/sanlk-reset*
%{_unitdir}/sanlk-resetd.service
%dir %attr(-,root,root) %{_localstatedir}/run/sanlk-resetd


%changelog
* Fri Oct 11 2024 yaoxin <yao_xin001@hoperun.com> - 3.9.4-1
- Update to 3.9.4:
  * Make the watchdog timeout configurable
  * wdmd: close watchdog when not used
  * userstorage: replace by PyPI userstorage
  * sanlock: skip short delay in delta lease reacquire
  * sanlock: use product_uuid for host name
  * wdmd: fix timing for iTCO_wdt
  * sanlock: save client_id in token and report in status and use in -C
  * sanlock: include startup message in syslog
  * sanlock: improve repeated ballots in paxos_acquire
  * sanlock: fix zero io timeout for direct requests
  * sanlock: fix release writing zero dblock values
  * sanlock: skip delay when same host acquires delta lease

* Wed Apr 12 2023 wangjie5911 <jiewangaw@isoftstone.com> - 3.8.5-1
- update to 3.8.5 and sync patch Add sanlk_reset command t options.

* Wed Mar 08 2023 xu_ping <xu_ping33@h-pattners.com> - 3.8.4-3
- fix sanlk-reset -t command error.

* Wed Oct 19 2022 lihaoxiang <lihaoxiang9@huawei.com> - 3.8.4-2
- sanlock:fix memory leak of lockspace renewal history

* Tue Nov 23 2021 yanglongkang <yanglongkang@huawei.com> - 3.8.4-1
- update to 3.8.4

* Tue Jun 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 3.8.2-2
- add buildrequire gcc.

* Thu Jan 14 2021 yanglongkang <yanglongkang@huawei.com> - 3.8.2-1
- update package to 3.8.2

* Fri Oct 30 2020 yanglongkang <yanglongkang@huawei.com> - 3.8.1-2
- remove python2 dependency

* Thu Jul 16 2020 yanglongkang <yanglongkang@huawei.com> - 3.8.1-1
- update package to 3.8.1

* Sat Nov 30 2019 zoushuangshuang<zoushuangshuang@huawei.com> - 3.6.0-7
- Package init
